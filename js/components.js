function AddComponent(obj, component)
{
    if (!obj.hasOwnProperty('components')) obj.components = [];
    obj.components.push(component);
}

function GetComponent(obj, componentName) {
   var c = obj.components.find(function(element) {
        return (element.name == componentName);
    });
    return (c == undefined ? null : c);
}


// =======================================
//  :: CursorMove
// =========================================

function CursorMove(obj, cursor) {
    c = {};
    c.name = 'CursorMove';
    // Require
    if(GetComponent(c.obj, 'PlayerFields')) AddComponent(c.obj, PlayerFields(c.obj));
    // Members
    c.obj = obj;
    c.cursor = cursor;
    c.field = GetComponent(c.obj, 'PlayerFields');
    // Update
    c.Update = function() {
        if(this.cursor.left.isDown) this.obj.x -= 10;
        if(this.cursor.right.isDown) this.obj.x += 10;
        if(this.cursor.up.isDown) this.obj.y -= 10;
        if(this.cursor.down.isDown) this.obj.y += 10;
    }
    
    return c;
}

// =======================================
//  :: PlayerFields
// =========================================

function PlayerFields(obj)
{
    c = {};
    c.name = 'PlayerFields';
    // Members
    c.velocity 
}