var selectedStage = 1;

Menu = {}

Menu.preload = function() {
    Menu.load.image('title', 'img/title.png');
    Menu.load.spritesheet('btn', 'img/titlebtn.png', 80, 80, 6);
    Menu.load.image('titlebg', 'img/titlebg.png');
}

Menu.create = function() {
    Menu.add.sprite(0, 0, 'titlebg');
    Menu.add.sprite(250, 170, 'title');
    
    this.b1 = Menu.add.button(200, 490, 'btn', this.stage1, this, 3, 0, 3);
    this.b2 = Menu.add.button(400, 490, 'btn', this.stage2, this, 4, 1, 4);
    this.b3 = Menu.add.button(600, 490, 'btn', this.stage3, this, 5, 2, 5);

    this.b1.anchor.x = this.b1.anchor.y = this.b2.anchor.x = this.b2.anchor.y = this.b3.anchor.x = this.b3.anchor.y = 0.5
}

Menu.update = function() {
    
}

Menu.stage1 = function() {
    selectedStage = 1;
    Menu.state.start("Game");
}

Menu.stage2 = function() {
    selectedStage = 2;
    Menu.state.start("Game");
}

Menu.stage3 = function() {
    selectedStage = 3;
    Menu.state.start("Game");
}
